package com.gta.kafka.producer;

import java.util.Properties;

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;

public class KafkaProducer {
	
	private Producer<Long, String> producer;
	
	public void createProducer( final String servers ) {
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, servers);
        props.put(ProducerConfig.CLIENT_ID_CONFIG, "KafkaExampleProducer");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        producer = new org.apache.kafka.clients.producer.KafkaProducer<>(props);
    }
	
	public void sendMessage(final String msg, final String topic) throws Exception {
        long time = System.currentTimeMillis();

        try {
        		final ProducerRecord<Long, String> record = new ProducerRecord<>(topic, time, msg);

            RecordMetadata metadata = producer.send(record).get();

            long elapsedTime = System.currentTimeMillis() - time;
            System.out.printf("sent record(key=%s value=%s) " + 
            					 "meta(partition=%d, offset=%d) time=%d\n",
            					 record.key(), record.value(), metadata.partition(),
            					 metadata.offset(), elapsedTime);
        } finally {
            producer.flush();
        }
    }
	
	public void closeProducerSession() throws Exception {
		producer.close();
	}
	
	
}
