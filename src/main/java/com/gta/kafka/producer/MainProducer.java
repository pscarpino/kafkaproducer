package com.gta.kafka.producer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

// http://cloudurable.com/blog/kafka-tutorial-kafka-producer/index.html

/*
 * java -jar  /Users/paoloscarpino/eclipse-workspace/KafkaProducer/target/KafkaProducer-0.0.1-SNAPSHOT-shaded.jar 127.0.0.1:9092 test1 10 5000 /Users/paoloscarpino/Downloads/prova_spark/yahoo_stocks.csv 
 */

public class MainProducer {

	private static List<List<String>> partitions;
	private static int partiotionElaboratedPosition = 0;
	private static KafkaProducer producer = new KafkaProducer();
	
	public static void main(String... args) throws Exception {
		String jarFileName = new java.io.File(MainProducer.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getName();

		if (args.length == 5) {
			System.out.println("servers: " + args[0]);
			System.out.println("topics: " + args[1]);
			System.out.println("num msg send sec: " + Integer.valueOf(args[2]));
			System.out.println("send delay (millisecond): " + Integer.valueOf(args[3]));
			System.out.println("Input File: " + args[4]);

			partitions = partitionFile( args[4], Integer.valueOf(args[2]) );
			
			producer.createProducer( args[0] );
			sendMessages(args[1], Integer.valueOf(args[3]));
			
		} else {
			System.err.printf("Usage: java -jar %s <servers> <topic> <num msg send sec (integer)> <send delay (millisecond)> <input file>\n", jarFileName);
			System.err.printf("\tE.g.: java -jar %s 127.0.0.1:9092,127.0.0.1:9093,127.0.0.1:9094 test1 10 5000 /Users/paoloscarpino/Downloads/prova_spark/yahoo_stocks.csv \n", jarFileName);
		}

	}

	public static void sendMessages( String topic, Integer delay ) {
		Timer t = new Timer();
		t.schedule(new TimerTask() {
		    @Override
		    public void run() {
		    		
	        		try {
	        			System.out.println("partiotionElaboratedPosition: " + partiotionElaboratedPosition);
	        			System.out.println("partitions.size(): " + partitions.size());
	        			
	        			List<String> list = partitions.get(partiotionElaboratedPosition);
			    		for (String row : list) {
			    			producer.sendMessage(row, topic);
			    		}
			    		partiotionElaboratedPosition++;
			    		
	        			if( partiotionElaboratedPosition == partitions.size() ) {
	        				producer.closeProducerSession();
		        			t.cancel();
		        		}
	        		}catch(Exception e) {
					e.printStackTrace();
				}	
		    }
		}, 0, delay);
	}
	
	private static List<List<String>> partitionFile( String fileName, int partitionSize ) throws Exception {
		File f = new File( fileName );
		
		List<String> lista = new ArrayList<String>();
		
		try( BufferedReader br = new BufferedReader(new FileReader( f )) ) {
		    for(String line; (line = br.readLine()) != null; ) {
		    		lista.add(line);
		    }
		}
		
	    List<List<String>> partitions = new ArrayList<>();

	    for (int i=0; i<lista.size(); i += partitionSize) {
	        partitions.add(lista.subList(i, Math.min(i + partitionSize, lista.size())));
	    }
	    return partitions;
	}
}
